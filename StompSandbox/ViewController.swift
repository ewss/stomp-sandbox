//
//  ViewController.swift
//  StompSandbox
//

import UIKit
import AKStompClient
import EVReflection

class ViewController: UIViewController, AKStompClientDelegate {

    var stomp = AKStompClient()
    var connected = false

    @IBOutlet weak var messageView: UITextView!
    @IBOutlet weak var url: UITextField!
    @IBOutlet weak var messageToSend: UITextField!
    @IBOutlet weak var channel: UITextField!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBAction func connect(_ sender: AnyObject) {
        if connected {
            stomp.disconnect()
        } else if hasValue(field: url) {
            UserDefaults.standard.setValue(url.text, forKey: "last_url")
            stompClientConnect()
        } else {
            popupMessage(title: "Alert", message: "URL is empty")
        }
    }
    
    func registerRequest() -> RegisterRequest {
        let req = RegisterRequest()
        req.__type = "newType"
        req.ApplicationName = "newApplicationName"
        req.Beat = "newBeat"
        req.MachineName = "newMachineName"
        req.SendInitialCallUpdate = true
        req.SendInitialUnitUpdate = false
        req.UnitNumbers = ["new1", "new2"]
        req.VehicleNumber = "newVehicleNumber"
        
        return req
    }
    
    func contractUri() -> ContractUri {
        let contract = ContractUri()
        contract.string.append("M/Infrastructure/TwoWay/IDispatchContract")
        return contract
    }
    
    func contractAction() -> ContractAction {
        let action = ContractAction()
        action.string.append("M/Infrastructure/TwoWay/IDispatchContract/Register")
        return action
    }

    func body() -> Body {
        let b = Body()
        b.body.append(registerRequest())
        return b
    }
    
    func register() {
        let nreq = Request()
        nreq.map = Map()
        let entry = Entry()
        
        entry.string.append(body())
        entry.string.append(contractUri())
        entry.string.append(contractAction())
        
        nreq.addEntry(entry)
        stomp.sendMessage(nreq.toJsonString(), toDestination: "test_queue", withHeaders: ["transformation" : "jms-map-json"], withReceipt: "test")
    }
    
    
    func mapmessage() {
//        let r = RegisterRequest()
//        print(r.toJSON().0!)
        
//        let mm = MapMessage(entry: [MapEntry(entries: ["body"])])
//        print(mm.serializeAsJSON().0!)
        let msg = "\("{\"map\":{\"entry\":[{\"string\":[\"body\",\"[{\"__type\":\"type\",\"ApplicationName\":\"application\",\"Beat\":\"beat\",\"MachineName\":\"machine\",\"SendInitialCallUpdate\":true,\"SendInitialUnitUpdate\":true,\"UnitNumbers\":[\"1\"],\"VehicleNumber\":\"vehicle#\"}]\"]},{\"string\":[\"contracturi\",\"value2\"]},{\"string\":[\"action\",\"value3\"]}]}}")"
        stomp.sendMessage(msg, toDestination: channel.text!, withHeaders: ["transformation" : "jms-map-json"], withReceipt: "test")
        
    }
    
    @IBAction func send(_ sender: AnyObject) {
//        mapmessage()
    
        
        if hasValue(field: messageToSend) && hasValue(field: channel) {
            stomp.sendMessage(messageToSend.text!, toDestination: channel.text!, withHeaders: ["transformation" : "jms-map-json"], withReceipt: "test")
            
        } else {
            popupMessage(title: "Alert", message: "Msg/Channel is empty")
        }
    }
    
    @IBAction func subscribe(_ sender: AnyObject) {
        if hasValue(field: channel) {
            stomp.subscribeToDestination(channel.text!)
            UserDefaults.standard.setValue(channel.text, forKey: "last_channel")
        } else {
            popupMessage(title: "Alert", message: "Channel is empty")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        url.resignFirstResponder()
        messageView.resignFirstResponder()
        channel.resignFirstResponder()
        messageToSend.resignFirstResponder()
    }
    
    func hasValue(field: UITextField) -> Bool {
        return (field.text != nil) && (field.text?.characters.count)! > 0
    }
    
    func popupMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func colorString(string: NSString, color: UIColor) -> NSMutableAttributedString {
        let stringAttr = NSMutableAttributedString.init(string: string as String)
        let range = string.range(of: string as String)
        stringAttr.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue , range: range)
        return stringAttr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageView.layer.borderColor = UIColor(white: 0.65, alpha: 1.0).cgColor
        messageView.layer.borderWidth = CGFloat(1.0)
        messageView.layer.cornerRadius = CGFloat(6.0)
        
        url.text = UserDefaults.standard.string(forKey: "last_url")
        channel.text = UserDefaults.standard.string(forKey: "last_channel")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func stompClientConnect() {
        if !connected {
            stomp.openSocketWithURLRequest(NSURLRequest(url: NSURL(string: url.text!) as! URL) as URLRequest, delegate: self,
                                           connectionHeaders: ["clientType": "REMOTE", "authkey": "n/a", "qrCode": "", "heart-beat": "0,10000"])
        } else {
            popupMessage(title: "Alert", message: "In a current session to \(stomp.value(forKey: "url"))")
        }
    }

    func stompClientDidConnect(_ client: AKStompClient!) {
        connected = true
        connectButton.setTitle("Disconnect", for: UIControlState.normal)
        popupMessage(title: "Success", message: "Connected to \(url.text!)")
//        stomp.subscribeToDestination("/topic/foo")
//        stomp.subscribeToDestination("/topic/foo", withSelector:"JMSType='test' OR JMSXGroupID='me'")
//        register()
//        mapmessage()
    }
    
    func stompClient(_ client: AKStompClient!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header:[String:String]?, withDestination destination: String) {
        print("JSON \(destination) --> \(jsonBody)")
//        let req = EVObject.init(dictionary: jsonBody)
//        print(req)
        
//        if destination == "the-destination-im-waiting-for" {
            if let jsonBody = jsonBody as? NSDictionary {
                let map = jsonBody.object(forKey: "map") as? NSDictionary
                print("map ->\(map)")
                
                if let dataArray = map?["entry"] as? NSArray {
                    print("Data items count: \(dataArray.count)")
                    
                    for entry in dataArray {
                        let obj = entry as? NSDictionary
                        for (key, value) in obj! {
                            print("Property: \"\(key as! String)\"")
                            print("Value: \(value)")
                            if let v = value as? NSDictionary {
                                print("Value count: \(v.count)")
                                for (k, vv) in v {
                                    print("k: \(k)")
                                    print("vv: \(vv)")
                                }
                            }
                            //print("Property: \"\(value as! String)\"")
                        }
                    }
                }

                
//                print("\(jsonBody)")
            }
//        }
    }
    
    func stompClient(_ client: AKStompClient!, didReceiveMessage body: String?, withHeader header:[String:String]?, withDestination destination: String) {
        print("Received header --> \(header)")
        print("Received message --> \(body)")
        
        if let msg = body {
            let from = "From: \(destination)\n"
            let newmsg = (msg + "\n")
            
            messageView.text = messageView.text + from + newmsg
            
//            let text = messageView.text as NSString
//            let range = text.range(of: text as String)
//            messageView.scrollRangeToVisible(range)
        }
    }
    
    func stompClientDidDisconnect(_ client: AKStompClient!) {
        connected = false
        stomp = AKStompClient()
        connectButton.setTitle("Connect", for: UIControlState.normal)


        popupMessage(title: "Alert", message: "Client connection lost")
//        if url.text != nil && (url.text?.characters.count)! > 0 {
//            DispatchQueue.main.async(execute: {
//                self.stompClientConnect()
//            })
//        }
    }
    
    func stompClientWillDisconnect(_ client: AKStompClient!, withError error: NSError) {
        print("stompClientWillDisconnect: \(error)")
    }
    
    func serverDidSendReceipt(_ client: AKStompClient!, withReceiptId receiptId: String) {
        print("Receipt --> \(description)")
    }
    
    func serverDidSendError(_ client: AKStompClient!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("Error --> \(description)")
        popupMessage(title: "Error", message: description)
        
    }
    
    func serverDidSendPing() {
        print("Received ping from server")
    }
}

