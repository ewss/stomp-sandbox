//
//  MapMessage.swift
//  StompSandbox
//

import Foundation
import EVReflection

enum ReturnCode {
    case Success
    case SystemError
    case SerializationError
}

struct ReturnStatus {
    var code: ReturnCode
    var description: String

    init(code: ReturnCode = .Success, description: String = "Success") {
        self.code = code
        self.description = description
    }
}



class RegisterRequest: EVObject {
    var __type: String?
    var ApplicationName: String?
    var Beat:String?
    var MachineName: String?
    var SendInitialCallUpdate: Bool?
    var SendInitialUnitUpdate: Bool?
    var UnitNumbers:[String]?
    var VehicleNumber: String?
    
}

class ContractUri: EVObject {
    var string: [String] = ["contracturi"]
}

class ContractAction: EVObject {
    var string: [String] = ["action"]
}

class Body: EVObject {
    var body: [EVObject] = []
}

class Entry: EVObject {
    var string: [EVObject] = []
}

class Map: EVObject {
    var entry: [Entry] = []
}

class Request: EVObject {
    var map: Map = Map()
    
    func addEntry(_ entry: Entry) {
        map.entry.append(entry)
    }
}






//
//protocol StompMessage {
//    func props() -> [String: Any]
//    func toJSON() -> (String?, ReturnStatus)
//}
//
//extension StompMessage {
//    func toJSON() -> (String?, ReturnStatus) {
//        do {
//            let data = try JSONSerialization.data(withJSONObject: props())
//            return (String(data:data, encoding: String.Encoding.utf8), ReturnStatus())
//        } catch let error {
//            print("error converting to json: \(error)")
//            return (nil, ReturnStatus(code: ReturnCode.SerializationError, description: error.localizedDescription))
//        }
//    }
//}
//
//struct MapEntry: StompMessage {
//    var entries: [String]
//    
//    internal func props() -> [String : Any] {
//        let properties = [
//            "entry": self.entries
//        ] as [String: Any]
//    
//        return properties
//    }
//}
//
//struct MapMessage: StompMessage {
//    var entry: MapEntry
//    
//    internal func props() -> [String : Any] {
//        let properties = [
//            "map": self.entry
//        ] as [String: Any]
//        
//        return properties
//    }
//}
//
//struct RegisterRequest: StompMessage {
//    internal func props() -> [String : Any] {
//       let properties = [
//        "__type": self.__type,
//         "ApplicationName": self.ApplicationName,
//         "Beat": self.Beat,
//         "MachineName": self.MachineName,
//         "SendInitialCallUpdate": self.SendInitialCallUpdate,
//         "SendInitialUnitUpdate": self.SendInitialUnitUpdate,
//         "UnitNumbers": self.UnitNumbers,
//         "VehicleNumber": self.VehicleNumber ] as [String : Any]
//        return properties
//    }
//
//    var __type = "RegisterRequest:M/CAD"
//    var ApplicationName = "Police"
//    var Beat = ""
//    var MachineName = "TROPPLTCSCHMITT"
//    var SendInitialCallUpdate = true
//    var SendInitialUnitUpdate = true
//    var UnitNumbers:[String] = ["UP18V", "UP19V"]
//    var VehicleNumber = ""
//    
////    func toJSON() -> String? {
////        let props = ["__type": self.__type,
////                     "ApplicationName": self.ApplicationName,
////                     "Beat": self.Beat,
////                     "MachioneName": self.MachineName,
////                     "SendInitialCallUpdate": self.SendInitialCallUpdate,
////                     "SendInitialUnitUpdate": self.SendInitialUnitUpdate,
////                     "UnitNumbers": self.UnitNumbers,
////                     "VehicleNumber": self.VehicleNumber ] as [String : Any]
////
////        do {
////            let data = try JSONSerialization.data(withJSONObject: props())
////            print(data)
////            return String(data:data, encoding: String.Encoding.utf8)
////        } catch let error {
////            print("error converting to json: \(error)")
////            return nil
////        }
////    }
//}
//
//
//
//struct Sentence {
//    var sentence = ""
//    var lang = ""
//    var length = 12
//    
//    func toJSON() -> String? {
//        let props = ["Sentence": self.sentence, "lang": lang, "length":self.length] as [String : Any]
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject: props, options: .prettyPrinted)
//            return String(data: jsonData, encoding: String.Encoding.utf8)
//        } catch let error {
//            print("error converting to json: \(error)")
//            return nil
//        }
//    }
//    
//}

